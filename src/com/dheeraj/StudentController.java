package com.dheeraj;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {
	@Value("#{countryOptions}") 
	private Map<String, String> countryOptions;

	@RequestMapping("/showForm")
	public String showForm(Model m)
	{
		m.addAttribute("student",new Student());
		m.addAttribute("theCountryOptions", countryOptions);
		return "student-form";
	}
	
	@RequestMapping("/processForm")
	String processForm(@ModelAttribute("student") Student st)
	{
		
		System.out.println(st.getFirstName()+st.getLastName()+st.getCountry());
		return "show-student-data";
	}
}
