package com.dheeraj;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Value;

public class Student {
	String firstName;
	String lastName;
	String country;
	LinkedHashMap countryOptions;


	Student() {
		countryOptions=new LinkedHashMap();
		countryOptions.put("IN","India");
		countryOptions.put("ET","Ethiopia");
		countryOptions.put("BR","Brazil");
		countryOptions.put("FR","Francee");
	}

	public LinkedHashMap<String, String> getCountryOptions() {
		return countryOptions;
	}

	public void setCountryOptions(LinkedHashMap<String, String> countryOptions) {
		this.countryOptions = countryOptions;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
