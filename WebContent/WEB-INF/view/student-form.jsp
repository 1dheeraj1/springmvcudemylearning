<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Student form details</title>
</head>
<body>
	<f:form action="processForm" modelAttribute="student">
	First name : <f:input path="firstName"/>
	last name : <f:input path="lastName"/>
	country:<f:select path="country">
				<f:options items="${theCountryOptions}"/>
			</f:select>
	<input type="submit">
	</f:form>
</body>
</html>